package jm.edu.utech.welcome.services;

import jm.edu.utech.welcome.contracts.IWelcomeService;

public class WelcomeService implements IWelcomeService {

	public String getWelcomeMessage(String name) {
		return "Welcome " + name;
	}

}
